<img src='images/banner.webp' alt="banner">
<h2 align='center'>
    <strong>Hi! I'm Simon. Glad to see you here!</strong> 👋 <br/>
    <strong>I'm full-stack developer</strong> 💻
</h2>
<hr/>

<p>
    <strong>
        I enjoy the process of developing web-products. Prefer to dive into modern and most useful technologies and methods, which simplify or extend developing process or the final product itself. I find the use of my work by the end user to achieve his goals the most enjoyable part of my work.
    </strong>
</p>

<h3><strong>Basic info about me:</strong></h3>
<ul>
    <li>Age: 23</li>
    <li>City: Moscow 🇷🇺</li>
    <li>Education: Software Engineer bachelor, Higher School of Economics, Moscow</li>
    <li>Languages:
        <ul>
            <li>Russian 🇷🇺 : native speaker</li>
            <li>English 🇬🇧 : B2 - IELTS 6.5 (approved March 2021)</li>
            <li>German 🇩🇪 : B2 - Goethe-Institut Zertifikat (approved March 2022)</li>
        </ul>
    </li>
</ul>

<hr/>

<h3>
    <strong>The list of my projects:</strong>
</h3>
<ul>
    <li>Front-end development of application for ordering products <strong>Samokat & Supermarket</strong> at Sberbank</li>
    <li>Front-end development of application <strong>Status-app</strong> for search, which aggregates user's request from others Sberbank apps like <strong>Delivery Club</strong>, <strong>Samokat & Sbermarket</strong> and <strong>SberMegaMarket</strong></li>
    <li>Full-stack development of speech recognition application at BSS <i>(actual work)</i></li>
    <li>Support overlay for online tournament broadcasting at <a href="https://twitch.tv/octoleague_official">OCTOLeague</a></li>
    <li>OCTOLeague's website developing <i>(in progress)</i></li>
</ul>

<hr/>

<h3><strong>Work experience</strong></h3>
<ul>
    <li>October 2022 - now: Middle full-stack developer at Banks Soft Systems.</li>
    <li>October 2020 - October 2022: Junior front-end developer at Sberbank.</li>
    <li>September 2018 - June 2021: Monitoring specialist at BCS Bank.</li>
</ul>

<hr/>

<h3><strong>Technologies I use in projects</strong></h3>
<ul>
    <li>
        <strong>Programming languages:</strong><br/>

![JavaScript](https://img.shields.io/badge/javascript-%23a6a223.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) ![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
    </li>
    <li>
        <strong>Libraries & frameworks:</strong><br/>
        
![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) ![React Router](https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=react-router&logoColor=white) ![React Hook Form](https://img.shields.io/badge/React%20Hook%20Form-%23EC5990.svg?style=for-the-badge&logo=reacthookform&logoColor=white) \
![Redux](https://img.shields.io/badge/redux-%23593d88.svg?style=for-the-badge&logo=redux&logoColor=white) ![Redux Toolkit](https://img.shields.io/badge/redux/toolkit-%23593d88.svg?style=for-the-badge&logo=redux&logoColor=white) \
![Chart.js](https://img.shields.io/badge/chart.js-F5788D.svg?style=for-the-badge&logo=chart.js&logoColor=white) ![Electron.js](https://img.shields.io/badge/Electron-191970?style=for-the-badge&logo=Electron&logoColor=white) \
![Next JS](https://img.shields.io/badge/Next-black?style=for-the-badge&logo=next.js&logoColor=white) ![Express.js](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB) \
![Vue.js](https://img.shields.io/badge/vuejs%20%28v2%29-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D)
    </li>
    <li>
        <strong>Bundlers & compilers:</strong><br/>
        
![Webpack](https://img.shields.io/badge/webpack-%238DD6F9.svg?style=for-the-badge&logo=webpack&logoColor=black) ![Babel](https://img.shields.io/badge/Babel-F9DC3e?style=for-the-badge&logo=babel&logoColor=black)
    </li>
    <li>
        <strong>Stylings:</strong><br/>
        
![Styled Components](https://img.shields.io/badge/styled--components-DB7093?style=for-the-badge&logo=styled-components&logoColor=white) \
![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white) ![SASS](https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white) \
 ![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white) ![MUI](https://img.shields.io/badge/MUI-%230081CB.svg?style=for-the-badge&logo=mui&logoColor=white) \
![LaTeX](https://img.shields.io/badge/latex-%23008080.svg?style=for-the-badge&logo=latex&logoColor=white)
    </li>
    <li>
        <strong>Testings:</strong><br/>
        
![cypress](https://img.shields.io/badge/-cypress-%23E5E5E5?style=for-the-badge&logo=cypress&logoColor=058a5e)
    </li>
    <li>
        <strong>Databases:</strong><br/>
        
![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) \
![Strapi](https://img.shields.io/badge/strapi-%232E7EEA.svg?style=for-the-badge&logo=strapi&logoColor=white)
    </li>
    <li>
        <strong>Environments:</strong><br/>
        
![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
    </li>
    <li>
        <strong>IDEs:</strong><br/>

![Visual Studio Code](https://img.shields.io/badge/VS%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
    </li>
    <li>
        <strong>Others:</strong><br/>

![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white) ![ESLint](https://img.shields.io/badge/ESLint-1637a1?style=for-the-badge&logo=eslint&logoColor=white) ![Prettier](https://img.shields.io/badge/Prettier-a68523?style=for-the-badge&logo=prettier&logoColor=white) \
![Jira](https://img.shields.io/badge/jira-%230A0FFF.svg?style=for-the-badge&logo=jira&logoColor=white)  ![Confluence](https://img.shields.io/badge/confluence-%23172BF4.svg?style=for-the-badge&logo=confluence&logoColor=white)  ![Swagger](https://img.shields.io/badge/-Swagger-%23Clojure?style=for-the-badge&logo=swagger&logoColor=white) \
 ![Postman](https://img.shields.io/badge/Postman-FF6C37?style=for-the-badge&logo=postman&logoColor=white) ![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
    </li>
</ul>

<h3><strong>I'm currently learning</strong></h3>

![Next JS](https://img.shields.io/badge/Next-black?style=for-the-badge&logo=next.js&logoColor=white) ![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=for-the-badge&logo=graphql&logoColor=white) ![NestJS](https://img.shields.io/badge/nestjs-%23E0234E.svg?style=for-the-badge&logo=nestjs&logoColor=white)
<h3><strong>Acquanted with</strong></h3>

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white) ![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white) ![scikit-learn](https://img.shields.io/badge/scikit--learn-%23F7931E.svg?style=for-the-badge&logo=scikit-learn&logoColor=white) ![TensorFlow](https://img.shields.io/badge/TensorFlow-%23FF6F00.svg?style=for-the-badge&logo=TensorFlow&logoColor=white) \
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white) ![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white) ![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=c-sharp&logoColor=white) ![C](https://img.shields.io/badge/c-%2300599C.svg?style=for-the-badge&logo=c&logoColor=white) \
![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white) \
![Grafana](https://img.shields.io/badge/grafana-%23F46800.svg?style=for-the-badge&logo=grafana&logoColor=white)

<hr/>

<h3><strong>Hobbies</strong></h3>
<ul>
    <li>Guitar 🎸</li>
    <li>Hockey 🏒</li>
    <li>Skateboarding 🛹</li>
    <li>Table games 🧩</li>
    <li>Classic music, Jazz & Rock 🎶</li>
</ul>

<hr>

<h3><strong>My social media & contacts</strong></h3>
<a href="https://vk.com/k_o_patich">
    <img src='images/vk.svg' width='32' height='32' alt='VK'>
</a>
<a href="https://t.me/ko_patich">
    <img src='images/telegram.svg' width='32' height='32' alt='Telegram'>
</a>
<a href="https://instagram.com/ko_patich">
    <img src='images/instagram.svg' width='32' height='32' alt='Instagram'>
</a>
<a href="https://discordapp.com/users/289137948207611905">
    <img src='images/discord.svg' width='32' height='32' alt='Discord'>
</a>
<a href="mailto:semen.gurin@list.ru">
    <img src='images/email.svg' width='32' height='32' alt='E-Mail'>
</a>
